# 단어 임베딩: Word2Vec와 GloVe

## <a name="intro"></a> 개요
이 포스팅에서는 텍스트 데이터 분석을 위해 Word2Vec와 GloVe 같은 단어 임베딩을 사용하는 방법을 설명한다. 단어 임베딩은 단어의 의미론적 그리고 구문론적 특징을 포착하는 단어의 수치 표현이다. 이들은 감정 분석, 텍스트 분류, 기계 번역 등과 같은 다양한 자연어 처리 작업에 유용하다.

이 포스팅을 학습하면 다음 작업을 수행할 수 있다.

- 단어 임베딩이 무엇이며 어떻게 훈련되는지를 이해할 수 있다.
- Word2Vec와 GloVe 모델을 사용하여 텍스트 말뭉치로부터 단어 임베딩 생성을 할 수 있다.
- Python과 라이브러리를 사용하여 텍스트 데이터 분석에 단어 임베딩을 적용할 수 있다.

이 포스팅을 따라가려면 다음 사항이 필요하다.

- Python과 데이터 분석에 대한 기본적인 이해
- 텍스트 편집기 또는 IDE(Visual Studio Code 또는 PyCharm 등)
- numpy, pandas, gensim, nltk 및 scikit-learn과 같은 패키지가 설치된 Python 환경

단어 임베딩의 세계로 뛰어들 준비가 되었나요? 시작해 봅시다!

## <a name="sec_02"></a> 단어 임베딩이란?
단어 임베딩은 단어의 의미론적 특징과 통사론적 특징을 포착하는 단어의 수치 표현이다. 이들은 감정 분석, 텍스트 분류, 기계 번역 등 다양한 자연어 처리 작업에 유용하다.

그런데 왜 단어 임베딩이 필요할까? 단어 자체를 텍스트 데이터 분석의 특징으로 사용할 수는 없을까?

단어를 특징으로 사용하는 것의 문제는 단어들이 이산적이고 희소하다는 것이다. 이것은 각각의 단어가 별개의 실체로 취급된다는 것을 의미하며, 단어들 간의 유사성이나 관련성에 대한 개념이 없다는 것을 의미한다. 예를 들어, "dog"와 "cat"은 둘 다 동물이지만, 텍스트에서 서로 다른 기호로 표현된다. 게다가, 한 언어에서 가능한 단어의 수는 매우 많고, 그들 중 대부분은 드물게 나타난다. 이것은 고차원적이고 희소 피처 공간으로 이어지고, 이것은 텍스트 데이터로부터 의미 있는 패턴을 학습하는 것을 어렵게 만든다.

단어 임베딩은 각각의 단어를 저차원적이고 밀도가 높은 벡터에 매핑하여 의미나 용도가 유사하거나 연관된 단어가 유사하거나 가까운 벡터를 갖도록 함으로써 이 문제를 해결한다. 예를 들어, "dog"는 벡터 [0.2, -0.1, 0.5, …]에, "cat"은 벡터 [0.3, -0.2, 0.4, …]에 매핑될 수 있다. 이러한 벡터는 가능한 단어의 수보다 훨씬 작으며, 단어의 의미론적 특징과 통사적 특징을 포착한다. 예를 들어, "dog"와 "cat"은 동물이기 때문에 유사한 벡터를 가지며, "dog"와 "bark"는 둘 다 개와 관련이 있기 때문에 유사한 벡터를 갖는다.

이러한 단어 임베딩은 어떻게 훈련될까? 텍스트 말뭉치로부터 단어 임베딩을 어떻게 생성할 수 있을까? 다음 절에서는 단어 임베딩을 훈련하는 데 사용되는 방법과 모델을 살펴볼 것이다.

## <a name="sec_03"></a> 단어 임베딩 훈련 방법
텍스트 말뭉치에서 단어 임베딩을 훈련하는 데 사용할 수 있는 많은 방법과 모델이 있다. 본 절에서는 가장 인기 있고 널리 사용되는 두 가지 모델인 Word2Vec와 GloVe에 초점을 맞출 것이다.

Word2Vec와 GloVe는 유사한 맥락에서 나타나는 단어들이 유사한 의미를 갖는 경향이 있다는 점에 착안한 것이다. 이들은 텍스트 말뭉치에 있는 단어들의 동시 출현 패턴으로부터 단어 임베딩을 학습하기 위해 서로 다른 알고리즘을 사용한다. GloVe는 행렬 분해(matrix factorization) 접근법을 사용하고, Word2Vec은 신경망(neural network) 접근법을 사용한다.

이러한 모델의 세부 사항으로 들어가기 전에 먼저 단어 임베딩 훈련에 사용되는 주요 개념과 용어를 살펴본다.

### 어휘와 말뭉치
어휘는 언어나 영역에서 사용되는 독특한 단어들의 집합이다. 예를 들어, 영어의 어휘는 영어에서 사용되는 모든 단어들로 구성된 반면, 스포츠의 어휘는 스포츠와 관련된 모든 단어들로 구성된다.

말뭉치는 단어 임베딩 훈련을 위한 자료의 소스로 사용되는 텍스트 문서의 모음이다. 예를 들어, 위키백과의 말뭉치는 위키백과에서 이용 가능한 모든 기사로 구성되어 있는 반면, 뉴스의 말뭉치는 온라인에 게재되는 모든 뉴스 기사로 구성되어 있다.

어휘와 말뭉치의 크기와 품질은 단어 임베딩의 품질과 적용 범위에 영향을 미친다. 더 크고 다양한 어휘와 말뭉치는 더 많은 단어와 그 의미를 포착할 수 있지만 처리하는 데 더 많은 컴퓨팅 자원과 시간이 필요하다. 더 작고 집중적인 어휘와 말뭉치는 더 빠르고 쉽게 처리할 수 있지만 일부 단어와 그 의미를 놓칠 수 있다.

### 동시 출현과 맥락
동시 출현(co-occurrence)은 텍스트 문서 또는 텍스트 창에서 두 단어가 함께 나타나는 빈도 또는 확률이다. 예를 들어, 문서에서 "dog"와 "cat"의 동시 출현은 이 두 단어가 동일한 문서에서 함께 나타나는 횟수이고, 크기 5 단어의 창에서 "dog"와 "cat"의 동시 출현은 이 두 단어가 동일한 문서에서 서로 5단어 이내에서 나타나는 횟수이다.

맥락(context)은 텍스트 문서 또는 텍스트 창에서 대상 단어를 둘러싸고 있는 단어들의 집합이다. 예를 들어, 문서에서 "dog"라는 단어의 컨텍스트는 "dog"와 동일한 문서에 나타나는 모든 단어의 집합인 반면, 크기 5 단어의 창에서 "dog"라는 단어의 맥락은 동일한 문서에서 "dog" 앞과 뒤에 나타나는 5 단어의 집합이다.

동시 출현과 맥락은  단어 임베딩을 학습하기 위하여 사용되는 주요 정보원이다. 자주 발생하거나 유사한 맥락을 공유하는 단어들은 유사한 의미나 특징을 갖는 경향이 있다는 아이디어이다. 예를 들어, "dog"와 "cat"은 "pat", "animal", "fur" 등 동시에 빈번하게 출현하고 유사한 맥락을 공유한다, 동물이며, 털을 가지고 있는 등 유사한 의미나 특징을 갖는다.

### 벡터와 차원
벡터는 다차원 공간에서 한 점이나 방향을 나타내는 수열이다. 예를 들어, 벡터 [0.2, -0.1, 0.5, …]는 다차원 공간의 한 점을 나타내며, 벡터의 숫자는 각 차원에 해당된다.

차원(dimension)은 벡터를 기술하거나 구별하는 데 사용될 수 있는 피처 또는 특성이다. 예를 들어, 벡터 [0.2, -0.1, 0.5, …]에서 첫 번째 차원 값 0.2는 벡터를 기술하거나 다른 벡터와 구별하는 데 사용될 수 있는 피처에 해당된다.

벡터와 차원은 단어 임베딩의 주요 구성 요소이다. 이 아이디어는 각 단어를 벡터로 표현하고, 각 차원은 단어의 특징 또는 특성에 대응된다. 예를 들어, "dog"라는 단어는 벡터 [0.2, -0.1, 0.5, …]로 표현될 수 있으며, 여기서 각 차원은 동물, 털을 가지고, 소리를 내는 등 단어의 피처 또는 특성에 대응된다.

### 유사도와 거리
유사도는 두 벡터가 다차원 공간에서 얼마나 가깝거나 관계가 있는지를 나타내는 척도이다. 예를 들어, 벡터 [0.2, -0.1, 0.5, …]와 [0.3, -0.2, 0.4, …]의 유사도는 이 두 벡터가 공간에서 얼마나 가깝거나 관계가 있는지를 나타내는 척도이다.

거리는 두 벡터가 다차원 공간에서 얼마나 멀리 있거나 다른지를 나타내는 척도이다. 예를 들어, 벡터 [0.2, -0.1, 0.5, …]와 [0.3, -0.2, 0.4, …]의 거리는 이 두 벡터가 공간에서 얼마나 멀리 있거나 다른지를 나타내는 척도이다.

유사도와 거리는 단어 임베딩을 평가하고 비교하는 데 사용되는 주요 지표이다. 벡터가 유사하거나 가까운 단어는 유사하거나 관련된 의미나 피처를 갖는 경향이 있는 반면, 벡터가 다르거나 거리가 떨어진 단어는 의미나 피처가 다르거나 관련이 없는 경향이 있다는 아이디어이다. 예를 들어 'dog'와 'cat'은 벡터가 유사하거나 가까운 관계가 있어 동물, 털이 있는 등 유사하거나 관련된 의미나 피처를 갖는 반면, 'dog'와 'car'는 벡터가 다르거나 먼 관계가 있어 동물 vs. 차량, 털 vs. 바퀴가 있는 등 의미나 피처 다르거나 관련이 없다.

코사인 유사도, 유클리드 거리, 맨해튼 거리 등 벡터의 유사도와 거리를 측정하는 여러 방법이 있다. 이 포스팅에서는 두 벡터 사이의 각도의 코사인으로 정의되는 코사인 유사도를 사용할 것이다. 코사인 유사도는 -1부터 1까지 값을 갖는다. 여기서 -1은 벡터의 서로 반대 방향이며, 0은 벡터의 직교하며, 1은 벡터가 같음을 뜻한다.

### Word2Vec와 GloVe
단어 임베딩 훈련에서 사용되는 주요 개념과 용어를 설명하였다. 이제 Word2Vec과 GloVe가 어떻게 작동하고 서로 어떻게 다른지 살펴보자.

Word2Vec은 텍스트 말뭉치에 있는 단어들의 동시 출현 패턴으로부터 단어 임베딩을 학습하는 신경망 모델이다. CBOW(Continuous Bag-of-Words)와 Skip-gram의 두 가지 아키텍처를 사용한다. CBOW 아키텍처에서는 모델이 문맥 단어로부터 대상 단어를 예측하는 반면, Skip-gram 아키텍처에서는 모델이 대상 단어로부터 문맥 단어를 예측한다. 모델은 예측된 단어와 실제 단어의 차이를 측정하는 손실 함수를 최적화하여 단어 임베딩을 학습한다.

GloVe는 텍스트 말뭉치에 있는 단어의 동시 출현 행렬로부터 단어 임베딩을 학습하는 행렬 분해(matrix factorization) 모델이다. 단어의 개별 맥락을 고려하는 로컬 접근 방식이 아닌 말뭉치의 전반적인 통계를 고려하는 글로벌 접근 방식을 사용한다. 이 모델은 단어 벡터의 내적과 동시 출현 횟수의 로그 간의 차이를 측정하는 손실 함수를 최적화하여 단어 임베딩을 학습한다.

Word2Vec과 GloVe 모두 장단점이 있다. Word2Vec은 더 빠르고 훈련하기 쉽지만, GloVe가 포착한 글로벌 정보 중 일부를 놓칠 수 있다. GloVe가 더 정확하고 포괄적이지만, 훈련하기에는 더 복잡하고 메모리 집약적일 수 있다. 일반적으로 두 모델은 다양한 자연어 처리 작업에 사용할 수 있는 고품질의 워드 임베딩을 생성한다.

다음 절에서는 Word2Vec와 GloVe 모델에서 Python과 라이브러리를 이용하여 텍스트 말뭉치에서 단어 임베딩을 생성하는 방법을 살펴본다.

## <a name="sec_04"></a> Word2Vec: 연속 Bag-of-Words와 Skip-Gram 모델
이 절에서는 Word2Vec 모델을 사용하여 Python과 라이브러리로 텍스트 말뭉치에서 단어 임베딩을 생성하는 방법을 배울 것이다. 또한 Word2Vec 모델의 두 가지 아키텍처인 CBOW(Continuous Bag-of-words)와 skip-gram에 대해서도 설명한다.

### 라이브러리 솔치와 임포트
Word2Vec 모델을 사용하려면 다음 라이브러리를 설치하고 임포트해야 한다.

- **numpy**: 과학 컴퓨팅과 배열 작업을 위한 라이브러리
- **pandas**: 데이터 분석과 조작을 위한 라이브러리
- **gensim**: 토픽 모델링과 자연어 처리를 위한 라이브러리
- **nltk**: 자연어 처리와 텍스트 분석을 위한 라이브러리
- **scikit-learn**: 기계 학습과 데이터 마이닝을 위한 라이브러리

터미널 또는 명령 프롬프트에서 **`pip`** 명령을 사용하여 이러한 라이브러리를 설치할 수 있다.

```bash
# Install the libraries
$ pip install numpy pandas gensim nltk scikit-learn
```

라이브러리를 설치한 후 Python 스크립트 또는 노트북으로 임포트할 수 있다.

```python
# Import the libraries
import numpy as np
import pandas as pd
import gensim
import nltk
from sklearn.metrics.pairwise import cosine_similarity
```

### 말뭉치의 로딩과 전처리
다음 단계는 단어 임베딩 훈련에 사용할 말뭉치를 로드하고 전처리하는 것이다. 이 포스팅에는 [여기](http://mlg.ucd.ie/datasets/bbc.html)에서 다운로드할 수 있는 BBC 뉴스 웹사이트의 뉴스 기사 2225개의 샘플 말뭉치를 사용할 것이다. 말뭉치는 CSV 파일에 저장되며, 각 행에는 뉴스 기사의 제목, 범주 및 내용이 포함된다.

[//]: # (<span style="color:red">"여기" url update 다시 찾을 것</span>)

<span style="color:red">[Simple Neural Network from scratch in Python](https://www.kaggle.com/code/ancientaxe/simple-neural-network-from-scratch-in-python)를 참고하여 예를 다시 작성하여야 할 것이다</span>

pandas 라이브러리를 사용하여 말뭉치를 로드할 수 있으며, 이 라이브러리는 데이터를 테이블 형식으로 포함하는 DataFrame 객체를 반환한다.

```python
# Load the corpus
corpus = pd.read_csv("bbc_news.csv")
# Print the first 5 rows of the corpus
print(corpus.head())
```

위 코드의 출력은 다음과 같다.

```
title	category	content
0	Ad sales boost Time Warner profit	business	Quarterly profits at US media giant TimeWarner ...
1	Dollar gains on Greenspan speech	business	The dollar has hit its highest level against the...
2	Yukos unit buyer faces loan claim	business	The owners of embattled Russian oil giant Yukos ...
3	High fuel prices hit BA's profits	business	British Airways has blamed high fuel prices for ...
4	Peru's ex-spy chief on public trial	business	The former head of Peru's intelligence service, ...
```

말뭉치에는 제목, 범주, 내용 등 세 개의 열이 포함되어 있다. 이 포스팅에서는 뉴스 기사의 실제 텍스트가 포함된 내용 열만 사용할 것이다. DataFrame에서 내용 열을 추출하여 문자열 목록으로 변환할 수 있다.

```python
# Extract the content column
content = corpus["content"]
# Convert the content column to a list of strings
content = content.tolist()
# Print the first element of the content list
print(content[0])
```

위 코드의 출력은 다음과 같다.

```
Quarterly profits at US media giant TimeWarner jumped 76% to $1.13bn (£600m) for the three months to December, from $639m year-earlier.
The firm, which is now one of the biggest investors in Google, benefited from sales of high-speed internet connections and higher advert sales.
TimeWarner said fourth quarter sales rose 2% to $11.1bn.
For 2005, TimeWarner is projecting operating earnings growth of around 5%, and also expects higher revenue.
TimeWarner has been looking to put the AOL TimeWarner corporate debacle behind it.
AOL TimeWarner was formed by the $106bn merger of the two firms in 2001, but subsequently became known for being one of the most disastrous business combinations in history.
The group dropped AOL from its name at the end of 2003, and has been looking to float its cable TV business, Time Warner Cable, on the stock market.
TimeWarner also said it would restate its accounts as part of efforts to resolve an inquiry into AOL by US market regulators.
It has already offered to pay $300m to settle charges, in a deal that is under review by the courts.
The company said it was unable to estimate the amount it needed to set aside for legal reserves, which it previously set at $500m.
It intends to adjust the way it accounts for a deal with German music publisher Bertelsmann's purchase of a stake in AOL Europe, which it had reported as advertising revenue.
It will now book the sale of its stake in AOL Europe as a loss on the value of that stake.
```

단어 임베딩 훈련에 콘텐츠 목록을 사용하기 전에 단어 임베딩의 품질에 영향을 미칠 수 있는 노이즈 또는 관련 없는 정보를 제거하기 위해 전처리해야 한다. 일반적인 전처리 단계 중 일부는 다음과 같다.

- **소문자화**: 모든 단어를 소문자로 변환하여 대문자 다른 단어를 동일한 단어로 취급한다.
- **토큰화(tokenization)**: 텍스트를 단어나 문장과 같이 더 작은 단위로 분할하여 각 단위를 개별적으로 처리할 수 있도록 한다
- **불용어(stopword) 제거**: "the", "a", "and" 등 매우 일반적이고 큰 의미를 갖지 않는 단어를 제거한다.
- **레미제이션(lemmatization)**: 단어를 기본 또는 어근 형태로 축소하여 굴절이 다른 단어를 같은 단어로 취급한다.

자연어 처리와 텍스트 분석을 위한 다양한 기능과 도구를 제공하는 `nltk` 라이브러리를 사용하여 이러한 전처리 단계를 수행할 수 있다. 또한 불용어 목록과 WordNet 레마타이저와 같은 nltk 라이브러리에서 사용하는 일부 리소스를 다운로드해야 한다.

```python
# Download the resources
nltk.download("stopwords")
nltk.download("wordnet")
```

리소스를 다운로드한 후 텍스트 문자열을 입력으로 받고 전처리된 단어 목록을 출력으로 반환하는 함수를 정의할 수 있다.

```python
# Define a preprocessing function
def preprocess(text):
    # Lowercase the text
    text = text.lower()
    # Tokenize the text into words
    words = nltk.word_tokenize(text)
    # Remove the stopwords
    words = [word for word in words if word not in nltk.corpus.stopwords.words("english")]
    # Lemmatize the words
    words = [nltk.stem.WordNetLemmatizer().lemmatize(word) for word in words]
    # Return the list of words
    return words
```

그런 다음 리스트 컴프리헨션를 사용하여 콘텐츠 리스트의 각 요소에 이 기능을 적용할 수 있다.

```python
# Preprocess the content list
content = [preprocess(text) for text in content]
# Print the first element of the content list
print(content[0])
```

위 코드의 출력은 다음과 같다.

```
['quarterly', 'profit', 'u', 'medium', 'giant', 'timewarner', 'jumped', '76', '%', '$', '1.13bn', '(', '£600m', ')', 'three', 'month', 'december', ',', '$', '639m', 'year-earlier', '.', 'firm', ',', 'one', 'biggest', 'investor', 'google', ',', 'benefited', 'sale', 'high-speed', 'internet', 'connection', 'higher', 'advert', 'sale', '.', 'timewarner', 'said', 'fourth', 'quarter', 'sale', 'rose', '2', '%', '$', '11.1bn', '.', '2005', ',', 'timewarner', 'projecting', 'operating', 'earning', 'growth', 'around', '5', '%', ',', 'also', 'expects', 'higher', 'revenue', '.', 'timewarner', 'looking', 'put', 'aol', 'timewarner', 'corporate', 'debacle', 'behind', '.', 'aol', 'timewarner', 'formed', '$', '106bn', 'merger']
```

> <span style="color:red">[bbc 데이터세트](http://mlg.ucd.ie/datasets/bbc.html)에서 다운로드 받은 데이터 세트는 사이트에서 기술한 것과 같이 이미 전처리를 수행한 것이다. 따라서 위의 전처리 과정을 수행할 필요가 없다. 따라서 다운로드 받은 파일 'bbc.terms`을 아래의 Python 코드로 수행하여 위와 같은 출력을 얻을 수 있다.

```python
dirpath = "/Users/yjlee/MyProjects/Python/bbc/"
filename = "bbc.terms"
filepath = dirpath+filename
# print("filepath = ", filepath)

# open file
f = open(filepath, mode="r")
# print(f)
lines = f.readlines()
terms = [ line.strip() for line in lines ] # remove '\n'
print(terms)
```

출력 파일:

```
['ad', 'sale', 'boost', 'time', 'warner', 'profit', 'quarterli', 'media', 'giant', 'jump', '76', '113bn', 'three', 'month', 'decemb', 'firm', 'on', 'biggest', 'investor', 'googl', 'benefit', 'highspe', 'internet', 'connect', ...]
```
</span>

### Word2Vec 훈련
말뭉치를 전처리하면 전처리된 내용을 사용하여 Word2Vec 모델을 학습시킬 수 있다. 먼저 벡터 크기(단어 벡터의 차원)와 윈도우 크기(한 문장 내에서 현재 단어와 예측된 단어 사이의 최대 거리)같은 원하는 매개변수로 Word2Vec 모델을 초기화해야 한다. 그런 다음 전처리된 내용에서 어휘를 구축하고 gensim.models.Word2Vec 클래스를 사용하여 모델을 학습시킬 수 있다.

```python
# Initialize the Word2Vec model
w2v_model = gensim.models.Word2Vec(size=100, window=5, min_count=1, workers=4)

# Build the vocabulary
w2v_model.build_vocab(content)
# Train the Word2Vec model
w2v_model.train(content, total_examples=w2v_model.corpus_count, epochs=30)
```

모델을 훈련시킨 다음에는 이 모델을 사용하여 어휘에 포함된 단어에 대한 단어 임베딩을 생성할 수 있다. 예를 들어, "apple"이라는 단어의 단어 벡터를 얻으려면 다음과 같이 한다.

```python
# Get the word vector for "apple"
vector = w2v_model.wv['apple']
print(vector)
```

<span style="color:red">출력 표시 요함</span>

이는 학습된 임베딩 공간에서 단어 "apple"을 나타내는 100차원 벡터를 출력할 것이다.

단어 임베딩의 품질은 말뭉치의 크기, 전처리 단계와 모델 파라미터와 같은 다양한 요소에 의존한다는 것을 명심해야 한다. 원하는 결과를 얻기 위해 다른 설정으로 실험해야 할 수도 있다.

### Word2Vec 평가
Word2Vec 모델에 의해 생성된 단어 임베딩의 품질을 평가하기 위해 여러 가지 방법을 사용할 수 있다. 일반적인 한 접근 방식은 `gensim` 라이브러리에서 제공하는 `evaluate_word_analogies` 함수를 사용하는 것이다. 이 함수는 "man:woman::king:queen"과 같은 유사성에 대해 가장 유사한 단어를 찾는 모델의 능력을 평가한다. 이 함수는 주어진 유사성 집합에서 모델의 정확도를 반환한다.

```python
# Evaluate the Word2Vec model on word analogies
accuracy = w2v_model.wv.evaluate_word_analogies('questions-words.txt')
print("Word analogy accuracy:", accuracy)
```

<span style="color:red">출력 표시 요함</span>

다른 방법은 `most_similar` 함수를 사용하여 주어진 단어에서 가장 유사한 단어를 찾아 인간의 판단과 비교하는 것이다. 이를 위해 WordSim-353이나 SimLex-999 같은 데이터 세트를 사용할 수 있다.

```python
# Evaluate the Word2Vec model on WordSim-353 dataset
wordsim353 = pd.read_csv('wordsim353.csv')
human_similarity = []
model_similarity = []

for i, row in wordsim353.iterrows():
    word1 = row['Word 1']
    word2 = row['Word 2']
    human_similarity.append(row['Human (mean)'])
    try:
        sim = w2v_model.wv.similarity(word1, word2)
        model_similarity.append(sim)
    except KeyError:
        model_similarity.append(0)
# Calculate the correlation between human judgments and model similarities
correlation = np.corrcoef(human_similarity, model_similarity)[0, 1]
print("Word similarity correlation (WordSim-353):", correlation)
```

<span style="color:red">출력 표시 요함</span>

이러한 방법을 사용하여 Word2Vec 모델을 평가함으로써 단어 간의 의미적 관계를 포착하는 성능을 평가하고 모델의 성능을 향상시키기 위해 필요에 따라 모델을 미세 조정하거나 매개변수를 조정할 수 있다.

## <a name="sec_05"></a> GloVe: 단어 표현을 위한 글로벌 벡터
이 절에서는 GloVe 모델을 사용하여 Python과 라이브러리로 텍스트 말뭉치에서 단어 임베딩을 생성하는 방법을 설명한다. 또한 알고리즘과 GloVe 모델의 직관에 대해서도 다룰 것이다.

### 라이브러리 설치와 임포트
GloVe 모델을 사용하려면 다음 라이브러리를 설치하고 임포트해야 한다.

- **numpy**: 과학 컴퓨팅과 배열 작업을 위한 라이브러리
- **pandas**: 데이터 분석과 조작을 위한 라이브러리
- **gensim**: 토픽 모델링과 자연어 처리를 위한 라이브러리
- **nltk**: 자연어 처리와 텍스트 분석을 위한 라이브러리
- **scikit-learn**: 기계 학습과 데이터 마이닝을 위한 라이브러리
- **glove-python**: Glove 모델을 훈련하고 사용하기 위한 라이브러리

터미널 또는 명령 프롬프트에서 pip 명령을 사용하여 이러한 라이브러리를 설치할 수 있다.

```bash
# Install the libraries
$ pip install numpy pandas gensim nltk scikit-learn glove-python
```

라이브러리를 설치한 후 Python 스크립트 또는 노트북으로 임포트할 수 있다.

```python
# Import the libraries
import numpy as np
import pandas as pd
import gensim
import nltk
from sklearn.metrics.pairwise import 
from glove import Corpus, Glove
```

### ### 말뭉치의 로딩과 전처리
다음 단계는 단어 임베딩 훈련에 사용할 말뭉치를 로드하고 전처리하는 것이다. 이 포스팅에는 [여기](http://mlg.ucd.ie/datasets/bbc.html)에서 다운로드할 수 있는 BBC 뉴스 웹사이트의 뉴스 기사 10,000개(?)의 샘플 말뭉치를 사용할 것이다. 말뭉치는 CSV 파일에 저장되며, 각 행에는 뉴스 기사의 제목, 범주 및 내용이 포함된다.

<span style="color:red">"여기" url update 다시 찾을 것</span>

pandas 라이브러리를 사용하여 말뭉치를 로드할 수 있으며, 이 라이브러리는 데이터를 테이블 형식으로 포함하는 DataFrame 객체를 반환한다.

```python
# Load the corpus
corpus = pd.read_csv("bbc_news.csv")
# Print the first 5 rows of the corpus
print(corpus.head())
```

위 코드의 출력은 다음과 같다.

```
title	category	content
0	Ad sales boost Time Warner profit	business	Quarterly profits at US media giant TimeWarner ...
1	Dollar gains on Greenspan speech	business	The dollar has hit its highest level against the...
2	Yukos unit buyer faces loan claim	business	The owners of embattled Russian oil giant Yukos ...
3	High fuel prices hit BA's profits	business	British Airways has blamed high fuel prices for ...
4	Peru's ex-spy chief on public trial	business	The former head of Peru's intelligence service, ...
```

말뭉치에는 제목, 범주, 내용 등 세 개의 열이 포함되어 있다. 이 포스팅에서는 뉴스 기사의 실제 텍스트가 포함된 내용 열만 사용할 것이다. DataFrame에서 내용 열을 추출하여 문자열 목록으로 변환할 수 있다.

```python
# Extract the content column
content = corpus["content"]
# Convert the content column to a list of strings
content = content.tolist()
# Print the first element of the content list
print(content[0])
```

위 코드의 출력은 다음과 같다.

```
Quarterly profits at US media giant TimeWarner jumped 76% to $1.13bn (£600m) for the three months to December, from $639m year-earlier.
The firm, which is now one of the biggest investors in Google, benefited from sales of high-speed internet connections and higher advert sales.
TimeWarner said fourth quarter sales rose 2% to $11.1bn.
For 2005, TimeWarner is projecting operating earnings growth of around 5%, and also expects higher revenue.
TimeWarner has been looking to put the AOL TimeWarner corporate debacle behind it.
AOL TimeWarner was formed by the $106bn merger of the two firms in 2001, but subsequently became known for being one of the most disastrous business combinations in history.
The group dropped AOL from its name at the end of 2003, and has been looking to float its cable TV business, Time Warner Cable, on the stock market.
TimeWarner also said it would restate its accounts as part of efforts to resolve an inquiry into AOL by US market regulators.
It has already offered to pay $300m to settle charges, in a deal that is under review by the courts.
The company said it was unable to estimate the amount it needed to set aside for legal reserves, which it previously set at $500m.
It intends to adjust the way it accounts for a deal with German music publisher Bertelsmann's purchase of a stake in AOL Europe, which it had reported as advertising revenue.
It will now book the sale of its stake in AOL Europe as a loss on the value of that stake.
```

단어 임베딩 훈련에 콘텐츠 목록을 사용하기 전에 단어 임베딩의 품질에 영향을 미칠 수 있는 노이즈 또는 관련 없는 정보를 제거하기 위해 전처리해야 한다. 일반적인 전처리 단계 중 일부는 다음과 같다.

- **소문자화**: 모든 단어를 소문자로 변환하여 대문자 다른 단어를 동일한 단어로 취급한다.
- **토큰화(tokenization)**: 텍스트를 단어나 문장과 같이 더 작은 단위로 분할하여 각 단위를 개별적으로 처리할 수 있도록 한다
- **불용어(stopword) 제거**: "the", "a", "and" 등 매우 일반적이고 큰 의미를 갖지 않는 단어를 제거한다.
- **레미제이션(lemmatization)**: 단어를 기본 또는 어근 형태로 축소하여 굴절이 다른 단어를 같은 단어로 취급한다.

자연어 처리와 텍스트 분석을 위한 다양한 기능과 도구를 제공하는 `nltk` 라이브러리를 사용하여 이러한 전처리 단계를 수행할 수 있다. 또한 불용어 목록과 WordNet 레마타이저와 같은 nltk 라이브러리에서 사용하는 일부 리소스를 다운로드해야 한다.

```python
# Download the resources
nltk.download("stopwords")
nltk.download("wordnet")
```

리소스를 다운로드한 후 텍스트 문자열을 입력으로 받고 전처리된 단어 목록을 출력으로 반환하는 함수를 정의할 수 있다.

```python
# Define a preprocessing function
def preprocess(text):
    # Lowercase the text
    text = text.lower()
    # Tokenize the text into words
    words = nltk.word_tokenize(text)
    # Remove the stopwords
    words = [word for word in words if word not in nltk.corpus.stopwords.words("english")]
    # Lemmatize the words
    words = [nltk.stem.WordNetLemmatizer().lemmatize(word) for word in words]
    # Return the list of words
    return words
```

그런 다음 리스트 컴프리헨션를 사용하여 콘텐츠 리스트의 각 요소에 이 기능을 적용할 수 있다.

```python
# Preprocess the content list
content = [preprocess(text) for text in content]
# Print the first element of the content list
print(content[0])
```

위 코드의 출력은 다음과 같다.

```
['quarterly', 'profit', 'u', 'medium', 'giant', 'timewarner', 'jumped', '76', '%', '$', '1.13bn', '(', '£600m', ')', 'three', 'month', 'december', ',', '$', '639m', 'year-earlier', '.', 'firm', ',', 'one', 'biggest', 'investor', 'google', ',', 'benefited', 'sale', 'high-speed', 'internet', 'connection', 'higher', 'advert', 'sale', '.', 'timewarner', 'said', 'fourth', 'quarter', 'sale', 'rose', '2', '%', '$', '11.1bn', '.', '2005', ',', 'timewarner', 'projecting', 'operating', 'earning', 'growth', 'around', '5', '%', ',', 'also', 'expects', 'higher', 'revenue', '.', 'timewarner', 'looking', 'put', 'aol', 'timewarner', 'corporate', 'debacle', 'behind', '.', 'aol', 'timewarner', 'formed', '$', '106bn', 'merger']
```

### GloVe로 단어 임베딩 생성
이제 필요한 라이브러리를 설치하고 말뭉치를 전처리했으므로, GloVe 모델을 이용한 단어 임베딩 생성을 진행할 수 있다. 먼저, 전처리된 말뭉치로부터 동시 출현 행렬을 생성해야 한다. 동시 출현 행렬은 지정된 컨텍스트 윈도우 내에서 단어의 동시 출현 빈도를 캡처한다.

```python
from nltk.tokenize import word_tokenize
from glove import Corpus

# Tokenize the preprocessed content
tokenized_content = [word_tokenize(text) for text in content]
# Create a GloVe corpus object
corpus = Corpus()
corpus.fit(tokenized_content, window=10)
# Define the parameters for training the GloVe model
glove = Glove(no_components=100, learning_rate=0.05)
glove.fit(corpus.matrix, epochs=30, no_threads=4, verbose=True)
# Add the word vectors to the corpus dictionary
glove.add_dictionary(corpus.dictionary)
# Save the trained GloVe model
glove.save('glove.model')
```

위의 코드에서는 먼저 `nltk`의 `word_tokenize` 함수를 사용하여 전처리된 콘텐츠를 토큰화한다. 그런 다음 GloVe Corpus 객체를 생성하고 토큰화된 콘텐츠에 적합시켜 10 단어로 구성된 맥락 창을 지정한다. 다음으로 100 차원과 학습률이 0.05인 GloVe 모델을 정의한다. 그런 다음 말뭉치의 동시 출현 행렬을 사용하여 모델을 훈련하고 4개의 스레드로 30개의 에포크 동안 실행한다. 마지막으로 단어 벡터를 코퍼스 사전에 추가하고 훈련된 GloVe 모델을 저장한다.

## <a name="sec_06"></a> 텍스트 데이터 분석을 위해 단어 임베딩 사용 방법
이 절에서는 Python과 그 라이브러리를 이용한 텍스트 데이터 분석을 위해 단어 임베딩을 사용하는 방법을 설명한다. 또한 자연어 처리 작업을 위한 단어 임베딩의 응용과 이점에 대해서도 다룰 것이다.

### 단어 임베딩 로딩과 탐색
Word2Vec 또는 GloVe 모델을 사용하여 단어 임베딩을 훈련한 후, 단어 임베딩 작업을 위한 다양한 기능과 도구를 제공하는 `gensim` 라이브러리를 사용하여 단어 임베딩을 로드하고 탐색할 수 있다. `gensim.models.KeyedVectors.load` 함수를 사용하여 파일에서 단어 임베딩을 로드할 수 있으며, 이 함수는 단어 벡터와 관련 단어를 포함하는 `KeyedVectors` 객체를 반환한다. 예를 들어, Word2Vec 모델을 사용하여 단어 임베딩을 훈련하고 `"word2vec.model"`이라는 이름의 파일에 저장했다면, 다음과 같이 로드할 수 있다.

```python
# Load the word embeddings from the file
word2vec = gensim.models.KeyedVectors.load("word2vec.model")
# Print the number of words and the size of the vectors
print(word2vec.vectors.shape)
```

위 코드의 출력은 다음과 같다.

```
(14986, 100)
```

이것은 단어 임베딩이 14,986개의 단어를 포함하고 각 단어는 100차원 벡터로 표현되었다는 것을 의미한다. 벡터와 단어를 각각 포함하는 numpy 배열인 `word2vec.vectors`와 `word2vec.index_to_key` 속성을 사용하여 단어 벡터와 관련 단어에 접근할 수 있다. 예를 들어, 인덱스 0에 있는 벡터와 단어에 다음과 같이 접근할 수 있다.

```python
# Access the vector and the word at the index 0
vector = word2vec.vectors[0]
word = word2vec.index_to_key[0]
# Print the vector and the word
print(vector)
print(word)
```

위 코드의 출력은 다음과 같다.

```
[ 0.01605013 -0.01782377 -0.01535295 -0.00967115 -0.00387475 -0.00560942
  -0.01089567 -0.00163341 -0.00617676 -0.00244503 -0.00072205 -0.0029849
   0.00455194 -0.00111833 -0.00105799 -0.00086723  0.00082587 -0.00127362
  -0.00079728 -0.00037452 -0.00028102 -0.00020184 -0.00014336 -0.00010206
  -0.00007258 -0.00005164 -0.00003672 -0.00002611 -0.00001858 -0.00001322
  -0.0000094  -0.00000668 -0.00000475 -0.00000338 -0.0000024  -0.00000171
  -0.00000122 -0.00000087 -0.00000062 -0.00000044 -0.00000031 -0.00000022
  -0.00000016 -0.00000011 -0.00000008 -0.00000006 -0.00000004 -0.00000003
  -0.00000002 -0.00000001 -0.00000001 -0.00000001 -0.00000001 -0.00000001
  -0.00000001 -0.00000001 -0.00000001 -0.00000001 -0.00000001 -0.00000001
  -0.00000001 -0.00000001 -0.00000001 -0.00000001 -0.00000001 -0.00000001
  -0.00000001 -0.00000001 -0.00000001 -0.00000001 -0.00000001 -0.00000001
  -0.00000001 -0.00000001 -0.00000001 -0.00000001 -0.00000001 -0.00000001
  -0.00000001 -0.00000001 -0.00000001 -0.00000001 -0.00000001 -0.00000001
  -0.00000001 -0.00000001 -0.00000001 -0.00000001 -0.00000001 -0.00000001]
  said
```

이는 `"said"`라는 단어가 대부분 매우 작은 값을 포함하는 100차원 벡터로 표현된다는 것을 의미한다. 또한 `word2vec.key_to_index`와` word2vec.get_vector` 메서드를 사용하여 단어와 벡터에 접근할 수 있으며, 이 메서드는 단어를 입력으로 받아 인덱스와 벡터를 각각 반환한다. 예를 들어, `"said"`라는 단어의 인덱스와 벡터에 다음과 같이 접근할 수 있다.

```python
# Access the index and the vector of the word "said"
index = word2vec.key_to_index["said"]
vector = word2vec.get_vector("said")
# Print the index and the vector
print(index)
print(vector)
```

`"said"`라는 단어가 인덱스 0에 있으므로 위 코드의 출력은 이전 코드와 동일하다.

### 단어 임베딩 탐색
일단 워드 임베딩을 로드하면 워드 임베딩 작업을 위한 다양한 기능과 도구를 제공하는 gensim 라이브러리를 사용하여 워드 임베딩을 탐색할 수 있다. 워드 임베딩으로 수행할 수 있는 일반적인 작업은 다음과 같다.

- **가장 유사한 단어 찾기**: 단어 또는 벡터를 입력으로 받아 입력과 가장 유사한 단어 리스트와 유사성 점수를 반환하는 `word2vec.most_similar` 메서드 사용할 수 있다. 예를 들어, "dog"와 가장 유사한 단어를 다음과 같이 찾을 수 있다.

```python
# Find the most similar words to the word "dog"
word2vec.most_similar("dog")
```

    위 코드의 출력은 다음과 같다.

```
[('cat', 0.8790221214294434),
 ('pet', 0.8530548810958862),
 ('animal', 0.8499484062194824),
 ('rabbit', 0.8330366611480713),
 ('puppy', 0.8268787860870361),
 ('horse', 0.823310136795044),
 ('pig', 0.819088339805603),
 ('sheep', 0.818766713142395),
 ('cow', 0.8167145252227783),
 ('bird', 0.8117983341217041)]
```

    즉, "cat", "pet", "animal" 등은 단어 임베딩에 따라 "dog"와 가장 유사한 단어임을 의미한다. "dog"라는 단어의 벡터와 같이 벡터와 가장 유사한 단어를 다음과 같이 찾을 수도 있다.

```python
# Find the most similar words to the vector of the word "dog"
word2vec.most_similar(word2vec.get_vector("dog"))
```

    위 코드의 출력은 이전 코드와 동일해야 하는데, 이는 "dog"라는 단어의 벡터가 "dog"라는 단어와 동일하기 때문이다.

- **가장 유사하지 않은 단어 찾기**: 부정적인 인수와 함께 단어 또는 벡터를 입력으로 받아 입력과 가장 유사하지 않은 단어 리스트와 유사성 점수를 반환하는 `word2vec.most_similar` 메서드를 사용할 수 있다. 예를 들어 "dog"와 가장 유사하지 않은 단어를 다음과 같이 찾을 수 있다.

```python
# Find the most dissimilar words to the word "dog"
word2vec.most_similar(negative="dog")
```

    위 코드의 출력은 다음과 같다.

```
[('£', -0.28180068731307983),
 ('$', -0.27943485975265503),
 ('%', -0.2708679437637329),
 ('euro', -0.264489650726318),
 ...
]
```

## <a name="summary"></a> 마치며
이 포스팅에서는 Word2Vec와 GloVe와 같은 단어 임베딩을 텍스트 데이터 분석에 사용하는 방법을 설명하였다. 단어 임베딩은 단어의 의미론적 그리고 구문론적 특징을 포착하는 단어의 수치 표현이다. 이들은 감정 분석, 텍스트 분류, 기계 번역 등과 같은 다양한 자연어 처리 작업에 유용하다.

또한 Python과 라이브러리를 사용하여 텍스트 말뭉치에서 단어 임베딩을 훈련하는 방법도 보였다. Word2Vec 모델의 두 가지 아키텍처, 즉 연속 단어 가방(CBOW)과 skip-gram, 그리고 Glove 모델의 알고리즘과 직관을 탐구했다. gensim 라이브러리를 사용하여 단어 임베딩을 로드하고 탐색하는 방법과 가장 유사하거나 다른 단어를 찾고 단어 간의 유사성 또는 거리를 계산하고 유사성을 해결하는 것 같은 단어 임베딩으로 일반적인 작업을 수행하는 방법을 살펴봤다.

워드 임베딩을 사용하면 소스 텍스트에서 쉽게 접근할 수 없는 더 많은 정보와 통찰력으로 텍스트 데이터 분석을 풍부하게 할 수 있다. 또한 워드 임베딩을 기능 또는 입력으로 사용하여 자연어 처리 모델의 성능과 정확도를 향상시킬 수 있다. 워드 임베딩은 텍스트 데이터 분석 프로젝트에서 더 나은 결과와 결과를 달성하도록 도와줄 수 있는 강력하고 다재다능한 도구이다.
