# 단어 임베딩: Word2Vec와 GloVe <sup>[1](#footnote_1)</sup>

> <font size="3">Word2Vec와 GloVe와 같은 단어 임베딩을 텍스트 데이터 분석에 사용하는 방법에 대해 알아본다.</font>

## 목차

1. [개요](./word-embeddings.md#intro)
1. [단어 임베딩이란?](./word-embeddings.md#sec_02)
1. [단어 임베딩 훈련 방법](./word-embeddings.md#sec_03)
1. [Word2Vec: 연속 Bag-of-Words와 Skip-Gram 모델](./word-embeddings.md#sec_04)
1. [GloVe: 단어 표현을 위한 글로벌 벡터](./word-embeddings.md#sec_05)
1. [텍스트 데이터 분석을 위해 단어 임베딩 사용 방법](./word-embeddings.md#sec_06)
1. [마치며](./word-embeddings.md#summary)

<a name="footnote_1">1</a>: [ML Tutorial 28 — Word Embeddings: Word2Vec and GloVe](https://levelup.gitconnected.com/ml-tutorial-28-word-embeddings-word2vec-and-glove-ef751c94d69c?sk=3dd5bb1fac4dbbd40650d159e578e932)를 편역하였습니다.

> <span style="color:red">**Note**: [Simple Neural Network in Python from Scratch](https://medium.com/@prxdyu/simple-neural-network-in-python-from-scratch-2814443a3050)를 참고하여 특히 예를 다시 작성하여야 할 것이다</span>